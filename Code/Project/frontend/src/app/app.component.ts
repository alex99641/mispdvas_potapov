import { Component } from '@angular/core';
import { FileServiceService } from './services/file-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  files = []
  title = 'my-project';
  fileToUpload: File = null

  constructor(
    private fileService: FileServiceService
  ) { }

  // sendFile(event: any) {
  //   const files: FileList = event.target.files;
  //   this.fileToUpload = files.item(0);
  //   const formData = new FormData();
  //   formData.append('file', this.fileToUpload, this.fileToUpload.name);
  //   this.fileService.sendFile(formData).subscribe(
  //     res => {
  //       console.log(res)
  //     }, err => {
  //       console.log(err)
  //     }
  //   )
  // }

  // getFile() {
  //   this.fileService.getFile().subscribe(
  //     res => {
  //       this.files.push(res.message)
  //     }
  //   )
  // }
}
