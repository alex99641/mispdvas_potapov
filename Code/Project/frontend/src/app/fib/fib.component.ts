import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fib',
  templateUrl: './fib.component.html',
  styleUrls: ['./fib.component.scss']
})
export class FibComponent implements OnInit {
  fibNumbers: number[] = [0, 1]
  n: number
  constructor() { }

  ngOnInit(): void {
  }

  getNumbers() {
    for (var i = 2; i < this.n; i++) {
      this.fibNumbers.push(this.fibNumbers[i - 2] + this.fibNumbers[i - 1]);
    }
  }
}
