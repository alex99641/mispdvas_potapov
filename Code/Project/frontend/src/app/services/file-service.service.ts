import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FileServiceService {
  BASE_URL: string = environment.url
  constructor(
    private http: HttpClient
  ) { }

  sendFile(file): Observable<any> {
    return this.http.post(`${this.BASE_URL}/api/upload/`, file);
  }
  getFile(): Observable<any> {
    return this.http.get(`${this.BASE_URL}/api`);
  }
  enc(pass: string): Observable<any> {
    const params = new HttpParams().set("pass", pass)
    return this.http.get(`${this.BASE_URL}/encrypt`, { params });
  }
  dec(pass: string): Observable<any> {
    const params = new HttpParams().set("pass", pass)
    return this.http.get(`${this.BASE_URL}/decrypt`, { params });
  }
}
