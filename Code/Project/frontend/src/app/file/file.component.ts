import { Component, OnInit } from '@angular/core';
import { FileServiceService } from '../services/file-service.service';
import { RSA } from 'src/app/script/rsa.js'

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss']
})
export class FileComponent implements OnInit {
  files = []
  title = 'my-project';
  fileToUpload: File = null
  q: number
  p: number
  n: number
  e: number
  d: number
  m: number
  password: string = ''

  constructor(
    private fileService: FileServiceService
  ) { }

  ngOnInit() {
    this.getFile()
  }

  sendFile(event: any) {
    const files: FileList = event.target.files;

    this.fileToUpload = files.item(0);
    const formData = new FormData();
    formData.append('file', this.fileToUpload, this.fileToUpload.name);

    this.fileService.sendFile(formData).subscribe(
      res => {
        alert(res.message)
      }, err => {
        console.log(err)
      }
    )
  }

  getFile() {
    this.fileService.getFile().subscribe(
      res => {
        this.files = res
      }
    )
  }

  encrypt() {
    if (this.password != null && this.password != '')
      this.fileService.enc(this.password).subscribe(
        res => {
          alert(res.message)
        }
      )
  }

  decrypt() {
    if (this.password != null && this.password != '')
      this.fileService.dec(this.password).subscribe(
        res => {
          alert(res.message)
        }
      )
  }
}
