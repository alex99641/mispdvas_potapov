import { Component, OnInit } from '@angular/core';
import * as bigInt from 'big-integer';

import { RSA } from 'src/app/script/rsa'


@Component({
  selector: 'app-rsa',
  templateUrl: './rsa.component.html',
  styleUrls: ['./rsa.component.scss']
})
export class RsaComponent implements OnInit {
  q: number
  p: number
  n: number
  e: number
  d: number
  m: number

  string: string = 'My String'
  constructor() { }

  ngOnInit(): void {
  }

  encrypt() {
    const e = bigInt(65537);
    let p = bigInt(this.p);
    let q = bigInt(this.q);
    let totient;

    do {
      totient = bigInt.lcm(
        p.prev(),
        q.prev()
      );
    } while (bigInt.gcd(e, totient).notEquals(1) || p.minus(q).abs().shiftRight(256 / 2 - 100).isZero());

    let n = p.multiply(q)
    let d = e.modInv(totient)

    console.log(d)
  }

}