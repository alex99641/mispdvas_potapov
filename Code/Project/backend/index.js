const express = require('express')
const fileupload = require('express-fileupload');
var fs = require('fs');
const cors = require('cors')
const crypto = require('crypto')
const PORT = 4000
const RSA = require('./RSA/rsa');
const bigInt = require('big-integer');
const app = express()
app.use(fileupload());
app.use(cors())

app.use('/static', express.static(__dirname + '/public'));

let keys = RSA.generate(250);
let algorithm = "aes-192-cbc"; //algorithm to use
let password = '';
const iv = crypto.randomBytes(16); // generate different ciphertext everytime
let key = null  //create key

app.post('/api/upload', (req, res) => {
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send({ message: 'No files were uploaded.' });
    }
    let myFile = req.files.file;
    console.log(myFile)
    // myFile.mv(`public/${myFile.name}`, function (err) {
    myFile.mv(`public/new.txt`, function (err) {    
        if (err)
            return res.status(500).send(err);

        res.send({ message: 'File uploaded!' });
    });
});

app.get('/encrypt', (req, res) => {
    password = req.query.pass
    saveInEncryptFile()
    res.send({ message: 'File has been encrypted' })
})

async function saveInEncryptFile() {
    fs.writeFile('public/encrypted.txt', '', function () { })
    var array = fs.readFileSync('public/new.txt').toString().split("\n");

    for (i in array) {
        let result = rsaEncrypt(array[i]) + '\n';

        fs.appendFile('public/encrypted.txt', cipher(result), function (err) {
            if (err) return console.log(err);
        });
    }
}

app.get('/decrypt', (req, res) => {
    password = req.query.pass
    saveInDecriptFile()
    res.send({ message: 'File has been decrypted' })
})


async function saveInDecriptFile() {
    var array = fs.readFileSync('public/encrypted.txt',).toString().split("\n");
    fs.writeFile('public/decrypted.txt', '', function () { })
    for (i in array) {
        let decryptedText = cipherDec(array[i])
        console.log(decryptedText)
        let result = rsaDecrypt(BigInt(decryptedText)) + '\n';
        fs.appendFile('public/decrypted.txt', result, function (err) {
            if (err) return console.log(err);
        });
    }
}


function cipher(text) {
    key = crypto.scryptSync(password, 'salt', 24);
    const cipher = crypto.createCipheriv(algorithm, key, iv);
    encrypted = cipher.update(text, 'utf8', 'hex') + cipher.final('hex'); // encrypted text
    return encrypted;
}

function cipherDec(text) {
    const decipher = crypto.createDecipheriv(algorithm, key, iv);
    let decrypted = decipher.update(text, 'hex', 'utf8') + decipher.final('utf8'); //deciphered text
    return decrypted;
}

function rsaEncrypt(message) {
    const encoded_message = RSA.encode(message);
    const encrypted_message = RSA.encrypt(encoded_message, keys.n, keys.e);
    return encrypted_message.toString()
}

function rsaDecrypt(message) {
    message = bigInt(message)
    console.log(message)
    const decrypted_message = RSA.decrypt(message, keys.d, keys.n);
    const decoded_message = RSA.decode(decrypted_message);
    return decoded_message
}


app.get('/api', (req, res) => {
    res.send([{ message: `http://localhost:4000/static/encrypted.txt` },
    { message: `http://localhost:4000/static/decrypted.txt` }]);
});

app.listen(PORT)