import { Component, OnInit } from '@angular/core';
import * as bigInt from 'big-integer';


@Component({
  selector: 'app-rsa',
  templateUrl: './rsa.component.html',
  styleUrls: ['./rsa.component.scss']
})
export class RsaComponent implements OnInit {
  q: number = 0;
  p: number = 0;
  n: number = 0;
  e: number = 0;
  d: number = 0;
  m: number = 0;

  string: string = 'My String'
  constructor() { }

  ngOnInit(): void {
  }

  encrypt() {
    const e = bigInt(65537);
    let p = bigInt(this.p);
    let q = bigInt(this.q);
    let totient;

    do {
      totient = bigInt.lcm(
        p.prev(),
        q.prev()
      );
    } while (bigInt.gcd(e, totient).notEquals(1) || p.minus(q).abs().shiftRight(256 / 2 - 100).isZero());

    let n = p.multiply(q)
    let d = e.modInv(totient)

    console.log(d)
  }

}